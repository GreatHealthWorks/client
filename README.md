# Client for simple demo app
Technologies Used 
- [x] **Ionic**
- [x] **Angular**
- [x] **Socket.io**
- [x] **JWT**

## Update Ionic if you are not in latest version
`npm uninstall -g ionic` to uninstall old ionic

`npm install -g ionic` to install latest ionic

## To see the app
`npm install` to install dependencies

`ionic serve` to run your local server

`ionic run android` to deploy to your phone



