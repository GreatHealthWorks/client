import lodash from 'lodash';
export abstract class PaginationHelper {
    static pagination<T>(data: Array<T>, pageSize: number, pageNumber: number, totalResults: number, ...properties: {propertyName:string, contact:(value:string, i:number)=>string}[] ): Array<T> {
        var items = new Array<T>();
        if (pageSize === 0) {
            if (pageNumber === 0) {
                for (let i = 0; i < totalResults; i++) {
                    items.push(lodash.cloneDeep(data[i % data.length]));
                    for (let property of properties) {
                        let propertyName = property.propertyName;
                        if (propertyName) {
                            items[items.length - 1][propertyName] = property.contact(data[i % data.length][propertyName], i + 1);
                        }
                    }
                }
            }
        }
        else if (pageSize > 0 && pageNumber >= 0) {
            let pos = pageNumber * pageSize;
            let maxInThisPage = pos + pageSize;
            for (; pos < Math.min(maxInThisPage, totalResults); pos++) {
                items.push(lodash.cloneDeep(data[pos % data.length]));
                for (let property of properties) {
                    let propertyName = property.propertyName;
                    if (propertyName) {
                        items[items.length - 1][propertyName] = property.contact(data[pos % data.length][propertyName], pos + 1);
                    }
                }
            }
        }
        return items;
    }
}

