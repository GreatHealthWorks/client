export interface Paginated<T>{
  items: Array<T>;
  cursor: string;
}