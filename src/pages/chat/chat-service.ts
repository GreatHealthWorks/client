import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import {Msg} from './msg-model';
import {Messages} from './messages-model';

@Injectable()
export abstract class ChatService{
    constructor() {
    }
    abstract sendMessage(msg: string): Promise<Msg>;
    abstract getMessages():Observable<Msg>;
    abstract getPreviousMessages(cursor: any, pageSize: number): Promise<Messages>;
  }