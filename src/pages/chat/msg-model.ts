export class Msg {
  public _id: string;
  public text: string;
  public from: string;
  public fromMe: boolean;
  public created: Date;
  constructor(obj: { _id:string, text:string , from:string, fromMe?: boolean, created?:Date }
  ) { 
    this._id = obj._id;
    this.text = obj.text;
    this.from = obj.from;
    this.fromMe = obj.fromMe || false;
    this.created = obj.created;
  };
}