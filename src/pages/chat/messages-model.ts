import { Paginated } from "../../common/paginated";
import { Msg } from "./msg-model"
export class Messages implements Paginated<Msg> {
    public items: Array<Msg>;
    public cursor: string;
    constructor(obj: { items: Array<Msg> , cursor:any}) {
        this.items = obj.items.map((p) => new Msg(p));
        this.cursor = obj.cursor;
    }
  }