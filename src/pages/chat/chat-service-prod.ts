import { Injectable } from '@angular/core';
import { Http, URLSearchParams, RequestOptions, Headers } from '@angular/http';
import { Socket } from 'ng-socket-io';
import { ChatService } from './chat-service';
import { Observable } from 'rxjs/Observable';
import { AppConfig } from "../../app/app.config";
import { Msg } from './msg-model';
import { Messages } from './messages-model';

@Injectable()
export class ChatServiceProd extends ChatService {
  constructor(private http: Http, private socket: Socket) {
    super();
  }
  sendMessage(msg: string): Promise<Msg> {
    return this.getToken()
      .then((token) => {
        let headers = this.getHeaders(token);
        let options = new RequestOptions({ headers });
        let socketId = this.socket.ioSocket.id;
        return this.http.post(`${AppConfig.url}/api/chat`, {msg, socketId}, options)
          .map((response) => new Msg(response.json()))
          .toPromise();
      });
  }
  getMessages(): Observable<Msg> {
    let observable = new Observable<Msg>(observer => {
      this.socket.on('receive-message', (data) => {
        observer.next(new Msg(data));
      });
    })
    return observable;
  }
  getPreviousMessages(cursor: any, pageSize: number): Promise<Messages> {
    return this.getToken()
      .then((token) => {
        let headers = this.getHeaders(token);
        let params: URLSearchParams = new URLSearchParams();
        params.set('cursor', cursor);
        params.set('pageSize', pageSize.toString());

        let options = new RequestOptions({ headers, params });
        return this.http.get(`${AppConfig.url}/api/chats`, options)
          .map((response) => new Messages(response.json()))
          .toPromise();
      });
  }

  private getToken(): Promise<string> {
    return new Promise((resolve, reject) => {
      let tokenStr = localStorage.getItem('userTokenKey');
      let token = undefined;
      if (tokenStr) {
        token = JSON.parse(tokenStr).token;
      }
      if (token) {
        resolve(token);
      }
      reject("UserIsNotLoggedIn");
    });

  }

  private getHeaders(token: string) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authentication', `Bearer ${token}`);
    return headers;
  }

}