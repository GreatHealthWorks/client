import { Component, OnInit } from '@angular/core';
import { NavController } from 'ionic-angular';

import { ChatService } from '../chat/chat-service';
import { Msg } from '../chat/msg-model';
import { Observer } from 'rxjs/Observer';
import { Observable } from 'rxjs/Observable';
import { UsersPage } from '../user/users';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'page-chat',
  templateUrl: 'chat.html'
})
export class ChatPage implements OnInit {
  msg: string;
  pageSize: number = 15;
  cursor: any;
  isInfiniteScrollEnabled: boolean = false;
  nothingToShow: boolean = false;
  chats: Array<Msg> = new Array<Msg>();
  subscription:Subscription;

  constructor(private navCtrl: NavController, private chatService: ChatService) {
  }

  ngOnInit(): void {

  }
  ionViewWillEnter() {
    this.startSearch().then(() => {
      this.subscription = this.chatService.getMessages().subscribe(message => {
        this.nothingToShow = false;
        this.chats.unshift(message);
      });
    }).catch((err) => {
      if (err === "UserIsNotLoggedIn") {
        this.navCtrl.push(UsersPage);
      }
    });
  }
  ionViewDidLeave(){
    if(this.subscription){
      this.subscription.unsubscribe();
    }
  }
  sendMessage(): void {
    if (this.msg !== null && this.msg.toString().trim() !== '') {
      this.chatService.sendMessage(this.msg);
      this.msg = "";
    }
  }
  startSearch(refresher = null) {
    this.isInfiniteScrollEnabled = false;
    this.cursor = undefined;
    this.chats = [];
    this.nothingToShow = false;
    return this.chatService
      .getPreviousMessages(this.cursor, this.pageSize)
      .then((data) => {
        for (let msg of data.items) {
          this.chats.push(msg);
        }
        this.cursor = data.cursor;
        this.isInfiniteScrollEnabled = this.cursor != null;
        this.nothingToShow = this.chats.length == 0;
        if (refresher) {
          refresher.complete();
        }
      });
  }

  doInfinite(infiniteScroll) {
    this.chatService
      .getPreviousMessages(this.cursor, this.pageSize)
      .then(data => {
        if (data) {
          this.chats = this.chats.concat(data.items);
          this.cursor = data.cursor;
          this.isInfiniteScrollEnabled = this.cursor != null;
          infiniteScroll.enable(this.isInfiniteScrollEnabled);
          infiniteScroll.complete();
        }
      });
  }

  keyPressHandler(key) {
    if (key === 13) {
      this.sendMessage();
    }
  }
}
