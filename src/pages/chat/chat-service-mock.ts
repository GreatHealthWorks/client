import { Injectable } from '@angular/core';
import { ChatService } from './chat-service';
import { PaginationHelper } from '../../common/pagination-helper';
import { Observable } from 'rxjs/Observable';
import {Msg} from './msg-model';
import {Messages} from './messages-model';
import 'rxjs/add/observable/interval';
@Injectable()
export class ChatServiceMock extends ChatService{
    private messages = [
        new Msg({_id:"id",text:"Hello", from:"user", fromMe:true, created: new Date()}),
        new Msg({_id:"id",text:"Hi", from:"user", created: new Date()}),
    ];
    constructor() {
        super();
    }
    sendMessage(msg: string):Promise<Msg> {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                resolve(new Msg({
                    _id: "1",
                    text: "Hello from mock",
                    from: 'anonymous',
                }));
            }, 100);
        });
    }

    getMessages():Observable<Msg> {
        return Observable.interval(1000).map(i=> new Msg({_id:`${i}`,from:`user${i}`,text:"hello",created:new Date()}));
    }

    getPreviousMessages(cursor: any, pageSize: number = 10): Promise<Messages> {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                if (!cursor) {
                    cursor = 0;
                }
                let result = PaginationHelper.pagination<Msg>(this.messages, pageSize, Number(cursor), 40, {propertyName:'userId', contact:(value:string, i:number)=>`${value}_${i}`},{propertyName:'user', contact:(value:string, i:number)=>`${value}${i}`});
                resolve(new Messages({ items: result, cursor: Number(cursor) + 1 }));
            }, 100);
        });
    }
  }