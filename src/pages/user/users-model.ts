import { Paginated } from "../../common/paginated";
import { User } from "./user-model";
export class Users implements Paginated<User> {
    public items: Array<User>;
    public cursor: string;
    constructor(obj: { items: Array<User> , cursor:any}) {
        this.items = obj.items.map((p) => new User(p));
        this.cursor = obj.cursor;
    }
  }