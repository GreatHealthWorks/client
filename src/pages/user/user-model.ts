export class User {
  public nickname: string;
  public avatarURL: string;
  constructor(obj: { nickname:string ,  avatarURL: string}

  ) { 
    this.nickname = obj.nickname;
    this.avatarURL = obj.avatarURL;
  };
}