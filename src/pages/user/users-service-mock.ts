import { Injectable } from '@angular/core';
import { User } from './user-model';
import { Users } from './users-model';
import { UsersService } from './users-service';
import { PaginationHelper } from '../../common/pagination-helper';
@Injectable()
export class UsersServiceMock extends UsersService{
    private users = [
        new User({nickname:"user", avatarURL:"https://randomuser.me/api/portraits/thumb/women/"}),
        new User({nickname:"user", avatarURL:"https://randomuser.me/api/portraits/thumb/men/"}),
    ];
    totalResults = 83;
    constructor() {
        super();
    }
    getUsers(cursor: any, pageSize: number = 10): Promise<Users> {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                if (!cursor) {
                    cursor = 0;
                }
                let result = PaginationHelper.pagination<User>(this.users, pageSize, Number(cursor), this.totalResults, {propertyName:'userId', contact:(value:string, i:number)=>`${value}_${i}`},{propertyName:'avatarURL', contact:(value:string, i:number)=>`${value}${i}.jpg`});
                resolve(new Users({ items: result, cursor: Number(cursor) + 1 }));
            }, 100);
        });
    }

    login(nickname: string, password: string): Promise<{token:string}>{
        return Promise.resolve({token: "MockTocken"});
    }
    logout():void{

    }
  }