import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, ViewController} from 'ionic-angular';
import { ChatPage } from '../chat/chat';
import { UsersService } from './users-service';
import { User } from './user-model';
@Component({
  selector: 'page-users',
  templateUrl: 'users.html'
})
export class UsersPage implements OnInit{
  pageSize: number = 10;
  cursor: any;
  isInfiniteScrollEnabled: boolean = false;
  nothingToShow: boolean = false;
  users:Array<User>;
  constructor(private navCtrl: NavController, private navParams: NavParams, private userService: UsersService, private view: ViewController) {
   
  }

  ngOnInit(): void {
    this.view.showBackButton(false);
    this.startSearch();
  }

  login(event, user) {
    //this is only the simplify the UI so that no need to do login and register users.
    this.userService.login(user.nickname,'password')
    .then(()=>{
      this.navCtrl.pop();
    });
  }

  startSearch(refresher = null): void {
    this.isInfiniteScrollEnabled = false;
    this.cursor = undefined;
    this.users = [];
    this.nothingToShow = false;
    this.userService
      .getUsers(this.cursor, this.pageSize)
      .then((data) => {
        this.users = data.items;
        this.cursor = data.cursor;
        this.isInfiniteScrollEnabled = this.cursor != null;
        this.nothingToShow = this.users.length == 0;
        if (refresher) {
          refresher.complete();
        }
      });
  }

  doInfinite(infiniteScroll) {
    this.userService
      .getUsers(this.cursor, this.pageSize)
      .then(data => {
        if (data) {
          this.users = this.users.concat(data.items);
          this.cursor = data.cursor;
          this.isInfiniteScrollEnabled = this.cursor != null;
          infiniteScroll.enable(this.isInfiniteScrollEnabled);
          infiniteScroll.complete();
        }
      });
  }
}
