import { Injectable } from '@angular/core';
import { Http, RequestOptions, URLSearchParams } from '@angular/http';
import { Users } from './users-model';
import { UsersService } from './users-service';
import { AppConfig } from "../../app/app.config";
import 'rxjs/add/operator/map';
@Injectable()
export class UsersServiceProd extends UsersService {
  constructor(private http: Http) {
    super();
  }
  getUsers(cursor: any, pageSize: number): Promise<Users> {
    let params: URLSearchParams = new URLSearchParams();
    params.set('cursor', cursor);
    params.set('pageSize', pageSize.toString());
    let options = new RequestOptions({ params });
    return this.http.get(`${AppConfig.url}/api/users`, options)
      .map((response) => new Users(response.json()))
      .toPromise();
  }
  login(nickname: string, password: string): Promise<{ token: string }> {
    return this.http.post(`${AppConfig.url}/api/login`, {nickname,password})
      .map((response) => response.json())
      .toPromise().then(
        (token)=>{
          localStorage.setItem('userTokenKey' , JSON.stringify({ token:token.token, nickname }));
          return token;
        }
      );
  }
  logout(){
    localStorage.removeItem('userTokenKey');
  }
}