import { Injectable } from '@angular/core';
import { Users } from './users-model';
@Injectable()
export abstract class UsersService{
    constructor() {
    }
    abstract getUsers(cursor: any, pageSize: number): Promise<Users>;
    abstract login(nickname: string, password: string): Promise<{token:string}>;
    abstract logout(): void;
  }