import { Component } from '@angular/core';
import { NavController, NavParams} from 'ionic-angular';
import { UsersService } from '../user/users-service';
import { ChatPage } from '../../pages/chat/chat';
@Component({
  selector: 'page-logout',
  templateUrl: 'logout.html',
})
export class LogoutPage{

  constructor(private navCtrl: NavController, private navParams: NavParams, private userService: UsersService) {

  }

  logout():void{
    this.userService.logout();
    this.navCtrl.setRoot(ChatPage);
  }
}
