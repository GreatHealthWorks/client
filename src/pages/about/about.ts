import { Component, OnInit } from '@angular/core';
import { NavController, NavParams} from 'ionic-angular';

@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})
export class AboutPage implements OnInit  {

  constructor(private navCtrl: NavController, private navParams: NavParams) {

  }

  ngOnInit(): void {
  }
}
