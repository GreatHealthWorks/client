import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';

import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { AboutPage } from '../pages/about/about';
import { UsersPage } from '../pages/user/users';
import { ChatPage } from '../pages/chat/chat';
import { LogoutPage } from '../pages/logout/logout';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { UsersService } from "../pages/user/users-service";
import { UsersServiceMock } from "../pages/user/users-service-mock";
import { UsersServiceProd } from "../pages/user/users-service-prod";

import { ChatService } from "../pages/chat/chat-service";
import { ChatServiceMock } from "../pages/chat/chat-service-mock";
import { ChatServiceProd } from "../pages/chat/chat-service-prod";

import { SocketIoModule, SocketIoConfig } from 'ng-socket-io';
import { AppConfig } from "./app.config";
const config: SocketIoConfig = { url: AppConfig.url, options: {} }; 

let useMock = false;
@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    UsersPage,
    ChatPage,
    LogoutPage,
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    SocketIoModule.forRoot(config),
    HttpModule,
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    UsersPage,
    ChatPage,
    LogoutPage,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    { provide: UsersService, useClass: useMock ? UsersServiceMock : UsersServiceProd },
    { provide: ChatService, useClass: useMock ? ChatServiceMock : ChatServiceProd },
  ]
})
export class AppModule { }
